/*
 * Copyright (c) 2021.  ʕʘ̅͜ʘ̅ʔ
 * Use of this source code is governed by a
 * MIT license that can be found in the LICENSE.
 */

import 'package:passwd_gen/passwd_gen.dart';

/// Generates a 13 character long password with a collection
/// of french characters. Then print entropy of password as integer.
void main() {
  final generator = PasswordService.latinFrench();
  final password = generator(13);
  print('Get entropy for the generated password: ${password.entropy.toInt()}');
}