/*
 * Copyright (c) 2021,2022.  ʕʘ̅͜ʘ̅ʔ
 * Use of this source code is governed by a
 * MIT license that can be found in the LICENSE.
 */

import 'package:passwd_gen/passwd_gen.dart';

/// Example how to exclude some characters that not present on some keyboards
/// https://gitlab.com/kurdy/passwd-gen/-/issues/3
void main() {
  final generator = PasswordService.latinBase();
  final excludedChar = [
    '£',
    '\$',
    '€',
    '\\'
  ];
  final password=generator.generatePassword(length: 29, excludeItems: excludedChar);
  print('Password with length of 29; without some excluded char: $password');
}