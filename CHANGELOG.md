
## 0.1.0

- Initial version.

## 1.0.0-rc.1

- First published version 

## 1.0.0-rc.2

- Follow tips of https://pub.dev/packages/passwd_gen/score to increase score
- Remove logging dependencies, which is not multiple platforms

## 1.0.0-rc.3
- Solve [issue #1](https://bitbucket.org/kurdy/passwd-gen/issues/1/error-when-exclude-list-is-egal-to-a)

## 1.0.0-rc.4
- Solve [issue #2](https://bitbucket.org/kurdy/passwd-gen/issues/2/password-length-with-exclude)

## 1.0.0-rc.5
- Change throw error in exception.

## 1.0.0
- Change to first release

## 1.0.1
- Migrate repo to gitlab
- Migrate from pedantic to lints
- Upgrade dependencies & Tests

## 1.1.0

- Added the EFF general short word list.

## 1.1.1

- dart format short word list

## 1.1.2

- Improve documentation as suggested in issue #3
