/*
 * Copyright (c) 2021.  ʕʘ̅͜ʘ̅ʔ
 * Use of this source code is governed by a
 * MIT license that can be found in the LICENSE.
 */

import 'package:passwd_gen/passwd_gen.dart';
import 'package:test/test.dart';

void main() {
  group('Tests for PasswordService', () {
    final generator = PasswordService.latinBase();

    setUp(() {
      // Additional setup goes here.
    });

    test('Test password generator without parameters', () {
      final pwd = generator.generatePassword();
      print('password: ${pwd.password} / ${pwd.toString()} / ${pwd.entropy}');
      expect(pwd.password.length, 15);
    });

    test('Test password generator with parameters length', () {
      final pwd = generator.generatePassword(length: 29);
      print('password: ${pwd.password} / ${pwd.toString()} / ${pwd.entropy}');
      expect(pwd.password.length, 29);
    });

    test('Test password generator using callable', () {
      final pwd = generator(13);
      print('password: $pwd');
      expect(pwd.password.length, 13);
    });

    test('Test password generator using parameters distribution', () {
      num pwdLength = 0;
      final keys = generator.getCollectionKeys();
      final dist = <String, int>{};
      for (var key in keys) {
        dist.putIfAbsent(key, () => key.length);
        pwdLength += key.length;
      }
      final pwd = generator.generatePassword(distribution: dist);
      print('password: ${pwd.password} / ${pwd.toString()} / ${pwd.entropy}');
      expect(pwd.password.length, pwdLength);
    });

    test('Test password generator with custom collection', () {
      final wordsGenerator = PasswordService();
      wordsGenerator.addCollectionOfPasswordItems('frenchWords', [
        'affaire',
        'année',
        'arme',
        'armée',
        'attention',
        'balle',
        'boîte',
        'bouche',
        'carte',
        'cause',
        'chambre',
        'chance',
        'chose',
        'classe',
        'confiance',
        'couleur',
        'cour',
        'cuisine'
      ]);
      final pwd = wordsGenerator.generatePassword(length: 5);
      pwd.separator = ' ';
      print('password: $pwd');
      expect(pwd.password.length, 5,
          reason: 'Password length should be 5 words');
    });

    test('Test password generator with collection french', () {
      final generator = PasswordService.latinFrench();
      final pwd = generator.generatePassword(length: 29);
      print('password: $pwd');
      expect(pwd.password.length, 29, reason: 'Password length should be 29');
      final found = CommonPasswordItemList.frenchCharacters.firstWhere(
          (element) => pwd.password.contains(element),
          orElse: () => '');
      expect(found.isNotEmpty, true,
          reason: 'We should find at least one french character.');
    });

    test('Test password generator with exclude', () {
      final generator = PasswordService.latinFrench();
      final excludedChar = [
        'â',
        'ë',
        'î',
        'ï',
        'ô',
        'ù',
        'û',
        'ü',
        'ÿ',
        'æ',
        'œ',
        '«',
        '»',
        '’'
      ];
      final pwd = generator.generatePassword(
          length: 29, excludeItems: excludedChar);
      print('password: $pwd');
      expect(pwd.password.length, 29, reason: 'Password length should be 29');
      final found = CommonPasswordItemList.frenchCharacters.firstWhere(
          (element) => pwd.password.contains(element),
          orElse: () => '');
      expect(found.isNotEmpty, true,
          reason: 'We should find at least one french character.');
      expect(
          excludedChar.fold(
              false,
              (previousValue, element) => previousValue =
                  previousValue as bool || pwd.password.contains(element)),
          false,
          reason: 'We shouldn\'t find excluded char');
    });

    // https://bitbucket.org/kurdy/passwd-gen/issues/1/error-when-exclude-list-is-egal-to-a
    test('Test bug with exclude', () {
      final generator = PasswordService.latinFrench();
      final excludedChar = CommonPasswordItemList.frenchCharacters;
      generator.generatePassword(excludeItems: excludedChar);
      // Should not throw exception.
    });

    // https://bitbucket.org/kurdy/passwd-gen/issues/2/password-length-with-exclude
    test('Test bug password length with exclude', () {
      final generator = PasswordService.latinFrench();
      final excludedChar = CommonPasswordItemList.frenchCharacters;
      final password = generator.generatePassword(
          excludeItems: excludedChar, length: 50);
      expect(password.password.length, 50, reason: 'Expect a length of 50');
    });

    test('Test password generator with collection german', () {
      final generator = PasswordService.latinGerman();
      final pwd = generator.generatePassword(length: 29);
      print('password: $pwd');
      expect(pwd.password.length, 29, reason: 'Password length should be 29');
      final found = CommonPasswordItemList.germanCharacters.firstWhere(
          (element) => pwd.password.contains(element),
          orElse: () => '');
      expect(found.isNotEmpty, true,
          reason: 'We should find at least one german character.');
    });

    test('Test password generator with collection italian', () {
      final generator = PasswordService.latinItalian();
      final pwd = generator.generatePassword(length: 29);
      print('password: $pwd');
      expect(pwd.password.length, 29, reason: 'Password length should be 29');
      final found = CommonPasswordItemList.italianCharacters.firstWhere(
          (element) => pwd.password.contains(element),
          orElse: () => '');
      expect(found.isNotEmpty, true,
          reason: 'We should find at least one italian character.');
    });

    test('Test password generator with custom spanish', () {
      final generator = PasswordService.latinSpanish();
      final pwd = generator.generatePassword(length: 29);
      print('password: $pwd');
      expect(pwd.password.length, 29, reason: 'Password length should be 29');
      final found = CommonPasswordItemList.spanishCharacters.firstWhere(
          (element) => pwd.password.contains(element),
          orElse: () => '');
      expect(found.isNotEmpty, true,
          reason: 'We should find at least one spanish character.');
    });

    test('Test password generator with collection Eff large list words', () {
      final generator = PasswordService.effLargeListWords();
      final pwd = generator.generatePassword(length: 10);
      print('password: $pwd / ${pwd.entropy}');
      expect(pwd.password.length, 10, reason: 'Password length should be 10');
    });

    test('Test password generator with collection Eff short list words', () {
      final generator = PasswordService.effShortListWords();
      final pwd = generator.generatePassword(length: 10);
      print('password: $pwd / ${pwd.entropy}');
      expect(pwd.password.length, 10, reason: 'Password length should be 10');
    });
  });

  group('Tests for Password', () {
    const passwordAsList = [
      's',
      'L',
      'p',
      'â',
      '0',
      'ÿ',
      't',
      'ê',
      'M',
      '3',
      'o',
      'ê',
      'â',
      '5',
      'Z',
      'a',
      'S',
      'ç',
      'Y',
      'v',
      '7',
      'K',
      'D',
      '5',
      's',
      '6',
      '7',
      'n',
      'ë'
    ];
    final password = Password(passwordAsList, 114);

    test('Test password creation', () {
      final isEqual = password.password.toString() == passwordAsList.toString();
      print('password length: ${passwordAsList.length}');
      print('password: ${password.toString()}');
      expect(isEqual, true,
          reason: 'Expect source password is equal to the password as String');
    });

    test('Test password entropy', () {
      print('entropy: ${password.entropy.toInt()}');
      expect(password.entropy.toInt(), 198,
          reason: 'Expect Bits of Entropy of 198=log2(114^29)');
    });

    test('Test passphrase', () {
      const passPhraseAsList = ['hello', 'mister', 'bond'];
      final passPhrase = Password(passPhraseAsList, 26, ' ');
      var passPhraseAsString = passPhrase.toString();
      print('passPhrase: $passPhraseAsString');
      expect(passPhraseAsString.contains(' '), true,
          reason: 'Expect a space char as separator');
      expect(passPhraseAsString.split(' ').length, passPhraseAsList.length,
          reason: 'Expect the same length.');
      passPhrase.separator = '.';
      passPhraseAsString = passPhrase.toString();
      print('passPhrase: $passPhraseAsString');
      expect(passPhraseAsString.contains('.'), true,
          reason: 'Expect a . char as separator');
      expect(passPhraseAsString.split('.').length, passPhraseAsList.length,
          reason: 'Expect the same length.');
    });
  });
}
