/*
 * Copyright (c) 2021.  ʕʘ̅͜ʘ̅ʔ
 * Use of this source code is governed by a
 * MIT license that can be found in the LICENSE.
 */

part of passwd_gen;

/// Represents a password, which is the result of
/// [PasswordService.generatePassword]
/// It can be obtained with [password] as [List<String>] or
/// with [toString] as [String]
/// [entropy] is the bits of entropy calculated from size of collection items
/// and password length .
class Password extends Object {
  late final List<String> _password;

  /// Separator placed between components for the password as a single String.
  late String separator;

  late final num _numberOfCollectionItems;
  late final double _entropy;

  double _log2(num x) => log(x) / ln2;

  /// Constructor for a password.
  ///
  /// [password] is the result of generated password
  /// [numberOfCollectionItems] Is the number of items from which the password
  /// was generated
  /// [separator] Is the separator char use to build a String. Often set to
  /// space when using words instead of characters.
  Password(List<String> password, num numberOfCollectionItems,
      [this.separator = '']) {
    _password = password.toList(growable: false);
    _numberOfCollectionItems = numberOfCollectionItems;
    //this.separator = separator;
    _entropy = _log2(
        pow(_numberOfCollectionItems.toDouble(), _password.length.toDouble()));
  }

  /// Password components as a list of Strings.
  ///
  /// To obtain the password as a single String, use [toString].
  List<String> get password => _password.toList(growable: false);

  String _makeString() {
    final result = _password.isNotEmpty
        ? _password.reduce((value, element) => value + separator + element)
        : '';
    return result;
  }

  /// Number of bits of entropy in the password.
  ///
  /// log2([Password._numberOfCollectionItems]^[Password.password] length)
  double get entropy => _entropy;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Password &&
          runtimeType == other.runtimeType &&
          _makeString() == other._makeString();

  @override
  int get hashCode => _makeString().hashCode;

  /// Password components concatenated together with [separator] between them.
  @override
  String toString() => _makeString();
}

/// Service use to generate [Password]. Passwords are generated from
/// collections of elements and a random generator that takes elements
/// to build the password.
/// A secure Random is used if supported otherwise the normal is used.
class PasswordService {
  late final Random _rand;
  final Map<String, List<String>> _collectionsPasswordItem =
      <String, List<String>>{};

  void _initRandom() {
    try {
      _rand = Random.secure();
      // ignore: avoid_catching_errors
    } on UnsupportedError catch (e) {
      print('UnsupportedError exception: $e');
      print('Use the classic random generator instead.');
      _rand = Random();
    }
  }

  void _initLatinBase() {
    _collectionsPasswordItem.putIfAbsent(
        'latinAlphabetLowerCase', () => CommonPasswordItemList.latinAlphabet);
    _collectionsPasswordItem.putIfAbsent(
        'latinAlphabetUpperCase',
        () => CommonPasswordItemList.latinAlphabet
            .map((item) => item.toUpperCase())
            .toList());
    _collectionsPasswordItem.putIfAbsent(
        'latinNumbers', () => CommonPasswordItemList.latinNumbers);
    _collectionsPasswordItem.putIfAbsent(
        'specialCharacters', () => CommonPasswordItemList.specialCharacters);
  }

  /// No collections initialized.
  ///
  /// The [addCollectionOfPasswordItems] method must be used to add one or
  /// more collections, before it can generate any passwords.
  PasswordService() {
    _initRandom();
  }

  /// Initialized with latin characters.
  ///
  /// Use latin characters and characters found on UK keyboard
  PasswordService.latinBase() {
    _initRandom();
    _initLatinBase();
  }

  /// Initialized with latin and French characters.
  ///
  /// Use latin characters and characters found on UK keyboard.
  /// Add french characters collection.
  PasswordService.latinFrench() {
    _initRandom();
    _initLatinBase();
    _collectionsPasswordItem.putIfAbsent(
        'frenchCharacters', () => CommonPasswordItemList.frenchCharacters);
  }

  /// Initialized with latin and German characters.
  ///
  /// Use latin characters and characters found on UK keyboard.
  /// Add german characters collection.
  PasswordService.latinGerman() {
    _initRandom();
    _initLatinBase();
    _collectionsPasswordItem.putIfAbsent(
        'germanCharacters', () => CommonPasswordItemList.germanCharacters);
  }

  /// Initialized the latin and Italian characters.
  ///
  /// Use latin characters and characters found on UK keyboard.
  /// Add italian characters collection.
  PasswordService.latinItalian() {
    _initRandom();
    _initLatinBase();
    _collectionsPasswordItem.putIfAbsent(
        'italianCharacters', () => CommonPasswordItemList.italianCharacters);
  }

  /// Initialized the latin and Spanish characters.
  ///
  /// Use latin characters and characters found on UK keyboard.
  /// Add spanish characters collection.
  PasswordService.latinSpanish() {
    _initRandom();
    _initLatinBase();
    _collectionsPasswordItem.putIfAbsent(
        'spanishCharacters', () => CommonPasswordItemList.spanishCharacters);
  }

  /// Initialized with the EFF's long list of words.
  ///
  /// This is the long list of 7776 words as defined by the
  /// [EFF](https://www.eff.org/deeplinks/2016/07/new-wordlists-random-passphrases).
  ///
  /// All words contain only the 26 lowercase letters a-z, except for these
  /// four words which also contains a hyphen: drop-down, felt-tip, t-shirt and
  /// yo-yo. The list contains both "yoyo" and "yo-yo" as two distinct words.
  ///
  /// If words are not desired, the _excludeItems_ parameter to
  /// [generatePassword] can be used to eliminate them from the generated
  /// password.

  PasswordService.effLargeListWords() {
    _initRandom();
    _collectionsPasswordItem.putIfAbsent('effLargeListWords',
        () => effLargeWordsList.values.toList(growable: false));
  }

  /// Initialized with the EFF's general short list of words.
  ///
  /// This is the first short list of 1296 words as defined by the
  /// [EFF](https://www.eff.org/deeplinks/2016/07/new-wordlists-random-passphrases).
  ///
  /// All words contain only the 26 lowercase letters a-z, except for "yo-yo"
  /// which also contains a hyphen. The list contains both "yoyo" and "yo-yo" as
  /// two distinct words.
  ///
  /// If words are not desired, the _excludeItems_ parameter to
  /// [generatePassword] can be used to eliminate them from the generated
  /// password.

  PasswordService.effShortListWords() {
    _initRandom();
    _collectionsPasswordItem.putIfAbsent('effShortListWords',
        () => effShortWordsList.values.toList(growable: false));
  }

  /// Add a collection of items.
  ///
  /// The [key] identify the collection and the [collection] are all the
  /// items that make up the collection.
  ///
  /// The entire collection should be consistent: either every member is a
  /// single character string, or every member is a string with two or more
  /// characters. Do not mix items with single and multiple characters in
  /// the same collection.

  void addCollectionOfPasswordItems(String key, List<String> collection) {
    _collectionsPasswordItem.putIfAbsent(key, () => collection);
  }

  /// Remove a collection of items
  /// [key] identify the collection
  void removeCollectionOfChars(String key) {
    _collectionsPasswordItem.remove(key);
  }

  /// Remove all collections of items
  void removeAllCollectionOfChars() {
    _collectionsPasswordItem.clear();
  }

  /// Get all keys as Iterable
  Iterable<String> getCollectionKeys() => _collectionsPasswordItem.keys;

  /// Generate [Password] using callable class
  Password call(num length) => generatePassword(length: length);

  /// Generate a new password.
  ///
  /// [length] is by default equal to 15
  /// [distribution] define the number of characters per collection
  /// It can throw [StateError] if the list of collection is empty
  Password generatePassword(
      {num length = 15,
      Map<String, int>? distribution,
      List<String>? excludeItems}) {
    final buffer = <String>[];
    final tmpCollectionsPasswordItem = _removeExcludedItems(excludeItems);
    final num collectionsLength = tmpCollectionsPasswordItem.length;
    if (tmpCollectionsPasswordItem.isEmpty) {
      throw Exception(
          'To generate a password, instance needs at least one collection of '
          'password items. You may use PasswordService.latinBase() or '
          'addCollectionOfPasswordItems().');
    }
    final slice = length ~/ collectionsLength;
    var reminder = length % collectionsLength;
    tmpCollectionsPasswordItem.forEach((key, collection) => {
          if (distribution != null && distribution.isNotEmpty)
            {
              buffer.addAll(_buildPassword(collection,
                  distribution.containsKey(key) ? distribution[key]! : 0))
            }
          else
            {
              buffer.addAll(_buildPassword(collection, slice + reminder)),
              reminder = 0
            }
        });
    buffer.shuffle(_rand);
    final password = Password(
        buffer,
        tmpCollectionsPasswordItem.values.fold(
            0, (previousValue, element) => previousValue += element.length));
    if (buffer.any((e) => 1 < e.length)) {
      // Set default separator to be a space, since password is not just made
      // up of single letters.
      //
      // For this to work consistently, collections must always entirely
      // contain only single letters, or entirely contain strings with
      // two or more characters. Never a mixture of single character strings
      // and multiple character strings. A better solution is to track
      // characters vs words as a property of each collection.
      password.separator = ' ';
    }
    return password;
  }

  // Return a copy of collection items without excluded items
  Map<String, List<String>> _removeExcludedItems(List<String>? excludeItems) {
    if (excludeItems != null && excludeItems.isNotEmpty) {
      final cItems = <String, List<String>>{};
      _collectionsPasswordItem.forEach((key, collection) {
        final cGrowable = collection.toList(growable: true);
        cGrowable.removeWhere((item) => excludeItems.contains(item));
        if (cGrowable.isNotEmpty) {
          cItems.putIfAbsent(key, () => cGrowable);
        }
      });
      return cItems;
    } else {
      return _collectionsPasswordItem;
    }
  }

  // Randomly selects values from a collection.
  //
  // Returns a list of [length] strings, randomly chosen from all the members
  // of [collection].

  List<String> _buildPassword(List<String> collection, num length) {
    final buffer = <String>[];
    for (var i = 0; i < length; i++) {
      final index = _rand.nextInt(collection.length);
      buffer.add(collection.elementAt(index));
    }
    return buffer;
  }
}

/// Define some commons items list
class CommonPasswordItemList {
  /// Latin alphabet lower case
  static const latinAlphabet = [
    'a',
    'b',
    'c',
    'd',
    'e',
    'f',
    'g',
    'h',
    'i',
    'j',
    'k',
    'l',
    'm',
    'n',
    'o',
    'p',
    'q',
    'r',
    's',
    't',
    'u',
    'v',
    'w',
    'x',
    'y',
    'z'
  ];

  /// Latin number
  static const latinNumbers = [
    '0',
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9'
  ];

  /// Special characters available on uk keyboard
  ///
  /// Note: If some characters are not suitable,
  /// for instance because they are not available on the keyboard
  /// or not obvious they can be excluded.
  /// Please have a look at the example: example_exclude.dart
  // https://gitlab.com/kurdy/passwd-gen/-/issues/3
  static const specialCharacters = [
    '!',
    '"',
    '£',
    '\$',
    '%',
    '^',
    '&',
    '*',
    '(',
    ')',
    '-',
    '_',
    '=',
    '+',
    '[',
    ']',
    '{',
    '}',
    ';',
    ':',
    '\'',
    '@',
    '#',
    '~',
    ',',
    '.',
    '<',
    '>',
    '/',
    '?',
    '€',
    '\\',
    '|'
  ];

  /// French characters set
  static const frenchCharacters = [
    'à',
    'â',
    'ç',
    'é',
    'è',
    'ê',
    'ë',
    'î',
    'ï',
    'ô',
    'ù',
    'û',
    'ü',
    'ÿ',
    'æ',
    'œ',
    '«',
    '»',
    '’'
  ];

  /// German characters set
  static const germanCharacters = ['ä', 'ö', 'ü', 'ß'];

  /// Italian characters set
  static const italianCharacters = ['à', 'è', 'é', 'ì', 'ò', 'ù'];

  /// Spanish characters set
  static const spanishCharacters = [
    'Á',
    'É',
    'Í',
    'Ñ',
    'Ó',
    'Ú',
    'Ü',
    'á',
    'é',
    'í',
    'ñ',
    'ó',
    'ú',
    'ü',
    '¿',
    '¡'
  ];
}
