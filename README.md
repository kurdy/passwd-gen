[![Pub](https://img.shields.io/pub/v/passwd_gen.svg)](https://img.shields.io/pub/v/passwd_gen)

# Password and passphrase generator.

A library to generate passwords. It includes a series of collections of items such as:
Latin characters as well as French, German, Italian, Spanish extensions and the usual special characters. 

It also includes the EFF's word list from the work of Joseph Bonneau and others. (See references below).

It allows to extend the collections with its own elements.

## Usage

See ./example for more examples

```dart
import 'package:passwd_gen/passwd_gen.dart';

void main() {
    final generator = PasswordService.latinBase();
    print('Password with length of 29; upper,lower case, numbers, special '
    'characters that are available with UK/US keyboard: ${generator(29)}');
}
```

```dart
import 'package:passwd_gen/passwd_gen.dart';

void main() {
    final generator = PasswordService.effLargeListWords();
    print('Passphrase based on EFF\'s large words list: ${generator(5)}');
}
```

```dart
import 'package:passwd_gen/passwd_gen.dart';

void main() {
    final generator = PasswordService.latinFrench();
    final password = generator(13);
    print('Get bits of entropy for the generated password: ${password.entropy.toInt()}');
}
```

```dart
import 'package:passwd_gen/passwd_gen.dart';

void main() {
  final generator = PasswordService.latinBase();
  final excludedChar = [
    '£',
    '\$',
    '€',
    '\\'
  ];
  final password=generator.generatePassword(length: 29, excludeItems: excludedChar);
  print('Password with length of 29; without some excluded char: $password');
}
```

## Use:
* From pub.dev [passwd_gen](https://pub.dev/packages/passwd_gen)
* Install see [Installing in pub.dev](https://pub.dev/packages/passwd_gen/install)

## References

* [EFF Dice-Generated Passphrases](https://www.eff.org/dice)
* [Deep Dive: EFF's New Wordlists for Random Passphrases](https://www.eff.org/deeplinks/2016/07/new-wordlists-random-passphrases)
* [Generate Passwords.org](https://generatepasswords.org/)
* [Password strength](https://en.wikipedia.org/wiki/Password_strength)
* [zxcvbn: Low-Budget Password Strength Estimation](https://www.usenix.org/conference/usenixsecurity16/technical-sessions/presentation/wheeler)
* [Goodbye to the Creator of the Password, And Also Goodbye to Passwords?](https://medium.com/asecuritysite-when-bob-met-alice/goodbye-to-the-creator-of-the-password-and-also-goodbye-to-passwords-f4c8e86ad47a)
* [DEDIS Advanced Crypto Library for Go](https://github.com/dedis/kyber)
* [NIST Digital Identity Guidelines](https://doi.org/10.6028/NIST.SP.800-63-3)
* [NIST Digital Identity Guidelines Enrollment and Identity Proofing](https://doi.org/10.6028/NIST.SP.800-63a)
* [NIST Digital Identity Guidelines Authentication and Lifecycle Management](https://doi.org/10.6028/NIST.SP.800-63b)
* [NIST Digital Identity Guidelines Federation and Assertions](https://doi.org/10.6028/NIST.SP.800-63c)